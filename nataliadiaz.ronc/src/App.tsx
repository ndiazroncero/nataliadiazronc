import React from 'react';
import logo from './logo.svg';
import './App.css';
import Componente from './components/principle/reactcomponente';
import ComponenteFuncional from './components/principle/reactcomponenteFuncion';
import Exam from './components/hooks/example3component'

import AcademicTable from './components/container/academictable';
import Example3 from './components/hooks/example3component';
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Ey! <code>src/App.tsx</code> estamos en react.
        </p>
        {/*<Componente unpropiedad={"DAM"} ></Componente>*/}
        {/*<ComponenteFuncional unpropiedad={"Desarrollo de Aplicaciones Multiplataforma"}></ComponenteFuncional>*/}

              <AcademicTable></AcademicTable>
              <Example3></Example3>
      </header>
    </div>
  );
}

export default App;
