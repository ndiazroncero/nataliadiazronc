import React, { useState } from 'react';
import { Academic } from '../../models/academic.class';
import TitleComponent from '../principle/academiccomponent';

const AcademicTable = () => {
	const defaultTitle = new Academic(
		'Desarrollo de aplicaciones multiplataforma',
		'Programacion de aplicaciones para cualquier plataforma'
	);
	return (
		<div>
			<div>
				Academics:
			</div>
			<TitleComponent academica={defaultTitle}></TitleComponent>
		</div>
	);

}
export default AcademicTable;
