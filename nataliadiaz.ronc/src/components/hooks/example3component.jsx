import React, { useState, useRef, useEffect, useContext } from 'react';
import axios from "axios";

const estado = React.createContext(null);
const Componente1Atributo1 = () => {
	const estadoC1 = useContext(estado);
	return (
		<div>
			<p>El componente 1 Atributo 1 TOKEN: {estadoC1.token}</p>
			<Componente2Atributo2></Componente2Atributo2>
		</div>
	);
}
const Componente2Atributo2 = () => {
	const estadoC2 = useContext(estado);
	return (
		<div>
			<p>El componente 2 Atributo 2 SESION: {estadoC2.sesion}</p>
		</div>
	);
}
function ComponenteCompletoContexto() {
	const objeto = {
		token: '0001',
		sesion: 1
	}
	const [objetoensi, setObjeto] = useState(objeto);
	function actualiza() {
		setObjeto({
			token: 'KDEN2546SANL',
			sesion: objetoensi.sesion + 1
		})
	}
	return (
		<div>
			{/*Proporcion contexto a todos los objetos que esten dentro. Inyectado como provider*/}
			<estado.Provider value={objetoensi}>
				Sesi�n: {estado.sesion}
				<Componente1Atributo1></Componente1Atributo1>
				<button onClick={actualiza}> Actualizar</button>
			</estado.Provider>
		</div>
	);
}


const Example3 = () => {
	const personaje = {
		nombre: 'Natalia',
		profesion: 'Desarrolladora Web'
	}
	const [varparaestado, setfuncionparacambiarlo] = useState(personaje);
	

	function opcion1() {
		setfuncionparacambiarlo({
			...varparaestado,
			nombre: varparaestado.nombre + ' Doble'
		})
	}
	const opcion2 = () => {
		setfuncionparacambiarlo({
			nombre: varparaestado.nombre + ' Doble2',
			profesion: 'Desarrolladora Web'
		})
	}

	const nivel = 1;
	const [varnivel, setNivel] = useState(nivel);
	const cambioNivel = () => {
		setNivel(varnivel+1)
	}
	const referencia = useRef()
	useEffect(() => {
		console.log('Cambio en el estado de example3component, useEffect salta')
		console.log(referencia)
	}, [varnivel])


	useEffect(async () => {
		await getUser();
	}, [varnivel] );
	const getUser = () => {
		return axios.get('https://randomuser.me/api').then(
			(response) => {
				if (response.status === 200) {
					alert(JSON.stringify(response.data));
				} else {
					throw new Error('Error al obtener usuario');
				}
			}
		).catch((e) => console.error('[error] ${e}'));
	}

	return (
		<div>
			<p>Nombre: {varparaestado.nombre} </p>
			<p>Profesion: {varparaestado.profesion} </p>
			<button onClick={opcion1}>
				Opcion1:
				
			</button>
			<p>{"opcion1:prevState => ({ ..prevState <<lo mismo que>> =>' } ({...varparaestado"}</p>
			<button onClick={opcion2}>Opcion2 </button>
			<h4 ref={referencia}>
				En el dom este es el elemento que indicamos como referencia
			</h4>
			<p>Nivel: {varnivel} Probando use effect y expulsa por consola al pulsar</p>
			<button onClick={cambioNivel}>cambioNivel </button>
			<ComponenteCompletoContexto></ComponenteCompletoContexto>
		</div>
	);
}
export default Example3;