import PropTypes from 'prop-types';
import { Academic } from '../../models/academic.class';

const TitleComponent = ({academica}) => {

	return (
		<div>
			<h2>Titulo Academico: {academica.title}</h2>
		</div>
	);

}
TitleComponent.propTypes = {
	academica: PropTypes.instanceOf(Academic)
};
export default TitleComponent;
