import React, { Component} from 'react';
import PropTypes from 'prop-types';
class Componente extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: 'Natalia'
		}
	}
	render() {
		return (
			<div>
				<p>Bienvenido: {this.state.name}</p>
				<p>Titulo: {this.props.unpropiedad}</p>
				<button onClick={this.addfullname}>Try full</button>
			</div>
		);
	}
	addfullname = () => {
		this.setState((prevState) => (
			{ name: prevState.name + ' D.R' }))
	}
}
// .jsx teclare types
Componente.propTypes = { unpropiedad: PropTypes.string, };
export default Componente;