import React, { useState } from 'react';
import PropTypes from 'prop-types';

const ComponenteFuncional = (props) => {
	/* creamos un estado privado para la funcion*/
	/** set name genera nuevos valores*/
	const [name, setname] = useState('Natalias')
	const addfullname = () => {
		setname(name+' D.R')
	}

	return (
		<div>
			<p>Bienvenido: {name}</p>
			<p>Titulo: {props.unpropiedad}</p>
			<button onClick={addfullname}>Try full</button>
		</div>
	);
	
}
ComponenteFuncional.propTypes = {
	unpropiedad: PropTypes.string
};
export default ComponenteFuncional;
