export class Academic {
	title = '';
	description = '';
	constructor(title, description) {
		this.title = title;
		this.description = description;
	}
}