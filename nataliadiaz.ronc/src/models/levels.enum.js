export const LEVELS = {
	ASPIRANTE: 'aspirant',
	BAJO: 'low',
	NORMAL: 'normal',
	ALTO: 'hight',
	EXPERTO: 'expert',
	NATIVO: 'native'
}