import { LEVELS } from "./levels.enum"

export class User {
	name = '';
	age = '';
	languages = '';

	constructor(name, age, languages) {
		this.name = name;
		this.age = age;
		this.languages = languages
	}
}